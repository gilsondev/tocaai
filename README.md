# Toca Ai

Esse projeto foi idealizado e desenvolvido no IV Encontro GITEC. Ele envolve
uma integração com o Twitter e Grooveshark.

A partir dos tweets o usuário poderá ouvir a música que deseja. Ele envia o tweet, e
o sistema a partir do hash ``#tocaaionline`` vai capturar a mesma e pesquisar o nome 
da música no Grooveshar, para assim inserir no playlist.

## Autores
- Felipe Vieira <felipe.tio@gmail.com>
- Gilson Filho <contato@gilsondev.com>
- Starlone Oliveiro <star.ill.nino@gmail.com>
