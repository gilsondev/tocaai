# -*- coding:utf8 -*-

from django.test import TestCase
from apps.music.models import Music

class MusicTest(TestCase):
  def setUp(self):
    pass

  def test_search(self):
    music = Music.search('love two time')
    self.assertTrue(isinstance(music,dict))
    self.assertTrue(isinstance(music['SongID'],int))

  def test_vote(self):
    music = {
        u'SongID': 23275826,
        u'Url': u'http://tinysong.com/FEuq',
        u'ArtistName': u'Dizzy Gillespie',
        u'AlbumName': u'Bahiana',
        u'AlbumID': 3513817,
        u'ArtistID': 7757,
        u'SongName': u'samba'
        }

    score = Music.vote(music)
    self.assertEqual(score,1)

  def test_top(self):
    music_list = Music.top(1)
    self.assertTrue(isinstance(music_list,list))
    self.assertTrue(isinstance(music_list[0],dict))
    self.assertTrue(isinstance(music_list[0]['SongID'],int))
    self.assertTrue(isinstance(music_list[0]['Score'],int))

  def test_play_best(self):
    music = Music.play_best()
    # TODO testar se adicionau na playlist
