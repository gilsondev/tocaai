# -*- coding:utf8 -*-

import urllib
import json
import ast
from redis import Redis

TINYSONG_KEY = '4ed25f883edba7b0b102e9888b4d1b5d'
RANK = 'tocaai'
REDIS = Redis()

class Music():
  @classmethod
  def search(cls, tags):
    """
    Busca na API uma música baseado nas tags
    """

    tags = urllib.quote(tags)
    f = urllib.urlopen('http://tinysong.com/b/%s?format=json&key=%s' % (tags, TINYSONG_KEY))
    music = json.loads(f.read())
    f.close()
    return music or None

  @classmethod
  def vote(cls, music):
    """
    Incrementa e salva a musica retornando a
    pontuação atual.
    """

    REDIS.set(music['SongID'], music)
    score = REDIS.zincrby(RANK, music['SongID'])
    return score

  @classmethod
  def play_best(cls):
    """
    Remove do rank a musica mais votada para
    ser executada.
    """

    music_list = cls.top(1)

    if music_list:
      music = music_list.pop()

      # TODO adicionar na playlist...

      REDIS.zrem(RANK, music['SongID'])
      return music
    else:
      return None

  @classmethod
  def top(cls, num=10):
    """
    Lista a num musicas mais votadas do rank.
    Default: top 10
    """

    if num > 0:
      num = -1 * num

    # lista ordenada do menor para o maior,
    # portanto o último elemento é o mais votado.
    music_list = REDIS.zrange(RANK, num, -1, withscores=True)

    top_list = []
    for music_id, score in music_list:
      # Faz o eval no dict de forma segura,
      # isto é, apenas se for um objeto
      music_str = REDIS.get(music_id) or 'None'
      music = ast.literal_eval(music_str)

      # Adiciona a pontuação atual da musica
      music[u'Score'] = int(score)
      top_list.append(music)

    # retorna a lista de musicas iniciando com a
    # mais votada.
    top_list.reverse()
    return top_list

